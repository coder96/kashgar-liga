export default {

	sidebarOpen: state => state.app.sidebar.open,
	tipbarOpen: state => state.app.tipbar.open,

	role: state => state.permission.role,

	routes: state => state.permission.permittedRoutes,
}
