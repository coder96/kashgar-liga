import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import getters from './getters'
import app from "./modules/app";
import match from "./modules/match";
import team from "./modules/team";
import permission from "./modules/permission";
import table from "./modules/table";
import style from "./modules/style";

Vue.use(Vuex);


const _store = new Vuex.Store({
  modules: {
    app,
    match,
    team,
    permission,
    table,
    style
  },
  getters,
  plugins: [createPersistedState({
    key: 'frog-admin'
  })]
});


export default _store;
