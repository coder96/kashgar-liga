import {request} from "@/api/request";

const state = {
    teams: [],
    team: {}
};

const getters = {
    teams : state => state.teams,
    team : state => state.team
};

const actions = {
    getTeams: ({commit, getters}) => {
        request(`seasons/${getters.season_id}/teams`).then(({data}) => {
            commit('SET_TEAMS', data )
        }).catch(e => console.error(e))
    },
    getTeam: ({commit, getters}, id) => {
        request(`seasons/${getters.season_id}/teams/${id}`)
            .then((response) => {
                commit('SET_TEAM', response.data )
            });
    },
};

const mutations = {
    'SET_TEAMS' (state, teams) {
        state.teams = teams;
    },
    'SET_TEAM' (state, team) {
        state.team = team;
    }
};

export default {
    state,
    getters,
    actions,
    mutations,
}
