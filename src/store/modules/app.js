import _axios, {setToken} from "@/plugins/axios";
import {request} from "@/api/request";

export default {
	state: {
		season_id: 1,
		calendar: {},
		appTitle: 'Кашгар лига',
		sidebar: {
			open: false,
		},
		tipbar: {
			open: false,
		},
		sync: true,
		is_authorised: false,
		access_token: '',
		user: {},
		players: [],
		bombardiers: [],
		violators: [],
		leagues: [],
		myLeagues: [],
		league: {},
		seasons: [],
		season: {},
		stages: [],
		cards: {yellow: [], red: []}
	},
	getters: {
		is_authorised: state => state.is_authorised,
		access_token: state => state.access_token,
		appTitle: state => state.appTitle,
		players: state => state.players,
		bombardiers: state => state.bombardiers,
		violators: state => state.violators,
		leagues: state => state.leagues,
		myLeagues: state => state.myLeagues,
		league: state => state.league,
		seasons: state => state.seasons,
		season: state => state.season,
		season_id: state => state.season_id,
		stages: state => state.stages,
		cards: state => state.cards,
	},
	mutations: {
		TOGGLE_SIDEBAR: state => {
			state.sidebar.open = !state.sidebar.open;
		},
		TOGGLE_TIPBAR: state => {
			state.tipbar.open = !state.tipbar.open;
		},
		SYNC: state => {
			state.sync = !state.sync;
		},
		SET_PLAYERS: (state, players) => {
			state.players = players;
		},
		SET_BOMBARDIERS: (state, bombardiers) => {
			state.bombardiers = bombardiers;
		},
		SET_VIOLATORS: (state, violators) => {
			state.violators = violators;
		},
		SET_LEAGUES: (state, leagues) => {
			state.leagues = leagues
		},
		SET_MY_LEAGUES: (state, leagues) => {
			state.myLeagues = leagues
		},
		SET_LEAGUE: (state, league) => {
			state.league = league
		},
		SET_SEASONS: (state, seasons) => {
			state.seasons = seasons
		},
		SET_SEASON: (state, season) => {
			state.season = season
			state.season_id = season.id
		},
		SET_STAGES: (state, stages) => {
			state.stages = stages;
		},
		'SET_CARDS'(state, data) {
			state.cards = data;
		},
		'LOG_IN'(state, {token, user}) {
			state.is_authorised = true;
			state.access_token = token;
			state.user = user;
		},
		'LOG_OUT'(state) {
			_axios.get('logout').then(() => {
				state.is_authorised = false
				setToken('');
			}).catch((error) => {
				if (error.response) {
					if (error.response.status === 401) {
						state.is_authorised = false
						setToken('');
					}
				} else {
					// Something happened in setting up the request that triggered an Error
					console.error('Error', error.message);
				}
			});
		},
	},
	actions: {
		getUser ({dispatch, commit, state}) {
			if (state.is_authorised) {
				_axios.get('user').then(resp => {
					commit('LOG_IN', {
						token: state.access_token,
						user: resp.data
					})
					dispatch('switchRole', resp.data.role || 'user')
				}).catch((error) => {
					if (error.response) {
						if (error.response.status === 401) {
							commit('LOG_OUT');
						}
					} else {
						// Something happened in setting up the request that triggered an Error
						console.error('Error', error.message);
					}
				});
			} else commit('LOG_OUT')
		},
		login: async ({dispatch, commit}, {email, password}) => {
			let response = await _axios.post('login', {email, password});
			if (response.data.status_code === 200 && response.data.access_token) {
				commit('LOG_IN', {
					token: response.data.access_token,
					user: response.data.user,
				});
				dispatch('switchRole', response.data.user.role || 'user')
				setToken(response.data.access_token);
				return true;
			}
			return false;
		},
		logout: ({commit}) => {
			commit('LOG_OUT');
			commit('switchRole', 'unauthorised');
		},
		register: async ({commit}, {name, email, password, password_confirmation}) => {
			let response = await _axios.post('register', {name, email, password, password_confirmation});
			if (response.data.error)
				return {message: response.data.error};
			else
				return {result: true, message: 'Вы успешно зарегистрировались!'}
		},
		getPlayers ({commit}) {
			request('players').then(({data}) => {
				commit('SET_PLAYERS', data)
			})
		},
		getRatings ({commit, getters}) {
			request(`players/ratings/${getters.season_id}`).then(({data}) => {
				commit('SET_BOMBARDIERS', data.bombardiers)
				commit('SET_VIOLATORS', data.violators)
			})
		},
		getLeagues ({commit}) {
			request(`leagues`).then(({data}) => {
				commit('SET_LEAGUES', data.data)
			})
		},
		getMyLeagues ({commit}) {
			request(`leagues`).then(({data}) => {
				commit('SET_MY_LEAGUES', data.data)
			})
		},
		setSeasons ({commit}, seasons) {
			commit('SET_SEASONS', seasons)
		},
		setLeague ({commit}, league) {
			commit('SET_LEAGUE', league)
		},
		setSeason ({commit, getters}, season) {
			commit('SET_SEASON', season)
			getters.leagues.map(league => {
				if (league.id === season.league_id) {
					commit('SET_LEAGUE', league)
					commit('SET_SEASONS', league.seasons)
				}
			})
		},
		getStages ({commit, getters}) {
			request(`seasons/${getters.season_id}/stages`).then(({data}) => {
				commit('SET_STAGES', data.data)
			})
		},
		getCards ({commit, getters}) {
			request(`players/misses/${getters.season_id}`).then(({data}) => {
				commit('SET_CARDS', data)
			})
		},
	}
}
