import {request} from "@/api/request";

const state = {
    lastMatches: {},
    nextMatches: {},
    session: false
};

const getters = {
    lastMatches : state => state.lastMatches,
    nextMatches : state => state.nextMatches
};

const actions = {
    getCurrentInfo: ({commit}) => {
        request('match', 'lastMatches')
            .then((response) => {
                commit('SET_LAST_MATCHES', response.data )
            });
        request('match','nextMatches')
            .then((response) => {
                commit('SET_NEXT_MATCHES', response.data )
            });
    }
};

const mutations = {
    'SET_LAST_MATCHES' (state, matches) {
        state.lastMatches = matches;
    },
    'SET_NEXT_MATCHES' (state, matches) {
        state.nextMatches = matches;
    }
};

export default {
    state,
    getters,
    actions,
    mutations,
}
