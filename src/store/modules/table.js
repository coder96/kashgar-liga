import {request} from "@/api/request";

const state = {
    tableTeams: []
};

const getters = {
    tableTeams : state => state.tableTeams
};

const actions = {
    getTableTeams: ({commit, getters}) => {
        request(`seasons/${getters.season_id}/table`)
            .then((response) => {
                commit('SET_TABLE_TEAMS', response.data )
            });
    }
};

const mutations = {
    'SET_TABLE_TEAMS' (state, tableTeams) {
        state.tableTeams = tableTeams;
    }
};

export default {
    state,
    getters,
    actions,
    mutations,
}
