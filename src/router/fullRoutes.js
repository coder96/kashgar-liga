import error from './error'

const fullRoutes = [
	{
		path: '/',
		name: 'home',
		components: {
			default: () => import('@/views/common/Home/index'),
			tip: () => import('@/views/common/Home/tip')
		},
		meta: {
			sideName: 'Главная',
			icon: 'home'
		}
	},
	{
		path: '/leagues/:id?',
		name: 'leagues',
		props: { default: true },
		components: {
			default: () => import('@/views/common/Leagues/index'),
		},
		meta: {
			sideName: 'Турниры',
			icon: 'trophy'
		}
	},
	{
		path: '/calendar',
		name: 'calendar',
		components: {
			default: () => import('@/views/common/Calendar/index'),
		},
		meta: {
			sideName: 'Календарь',
			icon: 'calendar'
		}
	},
	{
		path: '/table',
		name: 'table',
		components: {
			default: () => import('@/views/common/Table/index'),
		},
		meta: {
			sideName: 'Таблица',
			icon: 'table'
		}
	},
	{
		path: '/rating',
		name: 'rating',
		components: {
			default: () => import('@/views/common/Players/Rating'),
		},
		meta: {
			sideName: 'Рейтинг игроков',
			icon: 'linechart'
		}
	},
	{
		path: '/players/cards',
		name: 'cards',
		components: {
			default: () => import('@/views/common/Players/Cards'),
		},
		meta: {
			sideName: 'Карточки',
			class: 'cards'
		}
	},
	{
		path: '/teams',
		components: {
			default: () => import('@/views/common/Teams/index'),
		},
		meta: {
			sideName: 'Команды',
			icon: 'team'
		},
		children: [
			{
				hidden: true,
				path: '/',
				name: 'teams-index',
				props: true,
				component: () => import('@/views/common/Teams/Teams')
			},
			{
				hidden: true,
				path: ':id',
				name: 'team',
				props: true,
				component: () => import('@/views/common/Teams/team')
			},
		]
	},
	{
		path: '/stadiums',
		name: 'stadiums',
		components: {
			default: () => import('@/views/common/stadiums/Stadiums'),
		},
		meta: {
			sideName: 'Стадионы',
			class: 'Stadiums'
		}
	},
	{
		path: '/players',
		name: 'players',
		components: {
			default: () => import('@/views/common/Players/Players'),
		},
		meta: {
			sideName: 'Игроки',
			icon: 'user',
			role: 'root'
		}
	},
	{
		path: '/tournaments',
		name: 'tournaments',
		components: {
			default: () => import('@/views/common/Tournaments/index')
		},
		meta: {
			icon: 'Partition',
			sideName: 'Мои турниры',
			role: 'admin'
		}
	},
	{
		path: '/matches',
		name: 'matches',
		components: {
			default: () => import('@/views/common/Match/CreateMatch')
		},
		meta: {
			icon: 'time-circle',
			sideName: 'Матчи',
			role: 'admin'
		}
	},
	{
		path: '/matches/:id',
		name: 'match-view',
		hidden: true,
		components: {
			default: () => import('@/views/common/Match/View')
		},
		props: true,
		meta: {
			sideName: 'Матч'
		}
	},
	{
		path: '/match-result',
		name: 'match-details',
		components: {
			default: () => import('@/views/common/Match/MatchResultForm')
		},
		meta: {
			icon: 'edit',
			sideName: 'Результаты матча',
			role: 'admin'
		}
	},
	{
		path: '/icons',
		name: 'icons',
		hidden: true,
		components: {
			default: () => import('@/views/common/Icon/index'),
			tip: () => import('@/views/common/Icon/tip')
		},
		meta: {
			icon: 'crown',
		}
	},
	{
		path: '/register',
		name: 'register',
		components: {
			default: () => import('@/views/common/Register/Register'),
		},
		hidden: true
	},
	{
		path: '/login',
		name: 'login',
		components: {
			default: () => import('@/views/common/Login/Login'),
		},
		hidden: true
	},
	error,
];

export default fullRoutes;
