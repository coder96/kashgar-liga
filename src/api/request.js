import _axios from "@/plugins/axios";
request('sanctum', 'csrf-cookie')

export async function request(controller, method = '', data = {}, type = 'get') {
    let requestUrl = `${API_URL}${controller}` + (method ? `/${method}` : '')
    if (type === 'get') {
        let keys = Object.keys(data);
        keys.forEach((k) => {
            requestUrl += '&' + k + '=' + data[k];
        })
        return _axios.get(requestUrl)
    } else {
        return _axios.post(requestUrl, data)
    }
}
