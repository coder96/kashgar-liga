export function deepCopy(obj) {
    let result = Array.isArray(obj) ? [] : {};
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            if (typeof obj[key] === 'object') {
                result[key] = deepCopy(obj[key]);
            } else {
                result[key] = obj[key];
            }
        }
    }
    return result;
}

export function goalType(goal) {
    let res = '';
    switch (goal.Type) {
        case -1:
            res = 'Автогол'
            break
        case 1:
            res = 'Со штрафного'
            break
        case 2:
            res = 'Пенальти'
            break
    }
    return res
}

export function fullName(item) {
    if (item) return `${item.FirstName} ${item.LastName}`
    else return ''
}

export function date(item) {
    let date = new Date(item);
    let now = new Date()

    const month = (date.getMonth() + 1) + '';
    let textDate = `${date.getDate()}.${month.padStart(2, '0')}.${date.getFullYear()}`;
    if (date.getMonth() === now.getMonth() && date.getFullYear() === now.getFullYear()) {
        switch (true) {
            case date.getDate() === now.getDate():
                textDate = 'сегодня'
                break;
            case date.getDate() === now.getDate() + 1:
                textDate = 'завтра'
                break;
            case date.getDate() === now.getDate() - 1:
                textDate = 'вчера'
                break;
        }
    }

    return textDate;
}

export function time(date) {
    let dateLocal = new Date(date);
    const minutes = `${dateLocal.getMinutes()}`.padStart(2, '0');

    return `${dateLocal.getHours()}:${minutes}`;
}

export function dateTime (_date) {
    return date(_date) + ' | ' + time(_date)
}
