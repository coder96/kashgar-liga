import Vue from 'vue'
import locale from 'element-ui/lib/locale/lang/ru-RU'

import Element, {
	Loading,
	MessageBox,
	Message,
	Notification,
} from 'element-ui'

Vue.use(Element, {locale});

Vue.prototype.$loading = Loading.service;
Vue.prototype.$msgbox = MessageBox;
Vue.prototype.$alert = MessageBox.alert;
Vue.prototype.$confirm = MessageBox.confirm;
Vue.prototype.$prompt = MessageBox.prompt;
Vue.prototype.$notify = Notification;
Vue.prototype.$message = Message;
