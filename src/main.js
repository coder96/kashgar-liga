import './style/index.scss'
import './constants'
import '@/plugins/axios'

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/element-ui.js'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/ant-icon'
import AsyncComputed from 'vue-async-computed'
import '@/plugins/vue-echarts'
import '@/plugins/frog-ui'
import VueClipboard from 'vue-clipboard2'
import {jumpTo} from '@/utils/routerUtils'
import commonPlugin from '@/plugins/common'
import VueRx from 'vue-rx'
import _ from 'lodash'
import _axios, {setToken} from "@/plugins/axios";
import {fullName, goalType, date, time, dateTime} from "@/utils/commonUtils";

if (store.getters.access_token)
	setToken(store.getters.access_token)

Vue.config.productionTip = false;

Vue.use(commonPlugin);
Vue.use(AsyncComputed);
Vue.use(VueClipboard);
Vue.use(VueRx);
Vue.filter('fullName', fullName)
Vue.filter('goalType', goalType)
Vue.filter('date', date)
Vue.filter('time', time)
Vue.filter('dateTime', dateTime)

Vue.prototype.$vars = store.state.style.vars;
Vue.prototype.$themes = store.state.style.themes;
Vue.prototype.$app = store.state.app;
Vue.prototype.$jumpTo = jumpTo;
Vue.prototype.$routeState = store.state.routeState;
Vue.prototype.$loda = _;
Vue.prototype.$http = _axios;


new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app');
